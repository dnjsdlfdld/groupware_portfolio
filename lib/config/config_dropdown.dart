import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

const List<DropdownMenuItem<String>> dropdownPosition = [
  DropdownMenuItem(value: 'CEO', child: Text('사장')),
  DropdownMenuItem(value: 'VICE_PRESIDENT', child: Text('부사장')),
  DropdownMenuItem(value: 'DIRECTOR', child: Text('부장')),
  DropdownMenuItem(value: 'CONDUCTOR', child: Text('차장')),
  DropdownMenuItem(value: 'SECTION_CHIEF', child: Text('과장')),
  DropdownMenuItem(value: 'DEPUTY', child: Text('대리')),
  DropdownMenuItem(value: 'EMPLOYEE', child: Text('사원')),
];

const List<DropdownMenuItem<bool>> dropdownIsEnable = [
  DropdownMenuItem(value: true, child: Text('재직자')),
  DropdownMenuItem(value: false, child: Text('퇴사자')),
];

const List<FormBuilderFieldOption<bool>> dropdownApproval = [
  FormBuilderFieldOption(value: true, child: Text('승인')),
  FormBuilderFieldOption(value: false, child: Text('반려')),
];
