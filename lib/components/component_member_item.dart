import 'package:flutter/material.dart';
import 'package:groupware_manager_front/config/config_color.dart';
import 'package:groupware_manager_front/model/member_manage/member_manage_item.dart';

class ComponentMemberItem extends StatelessWidget {
  const ComponentMemberItem({super.key, required this.manageItem, required this.callback});

  final MemberManageItem manageItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        height: 200,
        padding: const EdgeInsets.all(30),
        child: Card(
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                flex: 2,
                  child: _buildPhoto()
              ),
              Expanded(
                flex: 3,
                  child: _buildProfile(),
              ),
            ],
          ),
        ),
      )
    );
  }

  Widget _buildProfile() {
    return Container(
      color: colorLightGray,
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Text('사원번호 : ${manageItem.id}'),
          Text('이름 : ${manageItem.name}'),
          Text('입사일 : ${manageItem.dateIn}'),
        ]
      ),
    );
  }

  Widget _buildPhoto() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Image.asset('assets/lionProfile.png'),
    );
  }
}
