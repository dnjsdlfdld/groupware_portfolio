import 'package:flutter/material.dart';

class ComponentRefreshButton extends StatefulWidget {
  const ComponentRefreshButton({super.key, this.action1Icon, this.action1Callback,});

  final IconData? action1Icon;
  final VoidCallback? action1Callback;

  @override
  State<ComponentRefreshButton> createState() => _ComponentRefreshButtonState();
}

class _ComponentRefreshButtonState extends State<ComponentRefreshButton> {
  @override
  Widget build(BuildContext context) {
    return IconButton(onPressed: widget.action1Callback, icon: Icon(widget.action1Icon));
  }
}
