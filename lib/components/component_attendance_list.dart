import 'package:flutter/material.dart';
import 'package:groupware_manager_front/model/attendance/all_attendance_list_item.dart';

class ComponentAttendanceList extends StatelessWidget {
  const ComponentAttendanceList(
      {super.key, required this.attendanceListItem, required this.callback});

  final AllAttendanceListItem attendanceListItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: callback,
        child: Container(
          height: 200,
          padding: const EdgeInsets.all(30),
          child: Card(
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(flex: 2, child: _buildPhoto()),
                Expanded(
                  flex: 3,
                  child: _buildProfile(),
                ),
              ],
            ),
          ),
        ));
  }

  Widget _buildProfile() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text('이름 : ${attendanceListItem.memberName}'),
            Text('상태 : ${attendanceListItem.attendanceType}'),
            Text('출근시간 : ${attendanceListItem.timeStart}'),
            Text('퇴근시간 : ${attendanceListItem.timeLeave}'),
          ]),
    );
  }

  Widget _buildPhoto() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Icon(Icons.person_outline, size: 80),
    );
  }
}
