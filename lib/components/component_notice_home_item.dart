import 'package:flutter/material.dart';
import 'package:groupware_manager_front/model/notice/notice_home_item.dart';

class ComponentNoticeHomeItem extends StatelessWidget {
  final NoticeHomeItem item;
  final VoidCallback callback;

  const ComponentNoticeHomeItem({super.key, required this.callback, required this.item});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Column(
                children: [
                  Text('${item.noticeTitle ?? '(제목없음)'}'),
                  Divider(thickness: 2)
                ],
              ),
            )
          ],
        )
    );
  }
}


// Container
// padding: const EdgeInsets.all(10),
// margin: const EdgeInsets.only(bottom: 10, left: 12, right: 12, top: 20),
// decoration: BoxDecoration(
// color: Colors.white12,
// borderRadius: BorderRadius.all(Radius.circular(10)),
// border: Border.all(color: const Color.fromRGBO(100, 20, 100, 20,))
// ),
// child: