import 'package:flutter/material.dart';
import 'package:groupware_manager_front/config/config_color.dart';
import 'package:groupware_manager_front/model/holiday/holiday_list.dart';

class ComponentHolidayList extends StatelessWidget {
  const ComponentHolidayList({super.key, required this.holidayList, required this.callback});

  final HolidayList holidayList;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
      height: 200,
      padding: const EdgeInsets.all(30),
      child: Card(
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
                flex: 1,
                child: _buildPhoto()
            ),
            Expanded(
              flex: 3,
              child: _buildProfile(),
            ),
          ],
        ),
      ),
      )
    );
  }

  Widget _buildProfile() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Row(
              children: [
                Text('등록번호 : ${holidayList.uploadId}'),
              ],
            ),
            Row(
              children: [
                Text('소속 : ${holidayList.memberFullName}'),
              ],
            ),
            Container(
              child: Row(
                children: [
                  Text('제목 : ${holidayList.title}'),
                ],
              ),
            ),
          ]
      ),
    );
  }

  Widget _buildPhoto() {
    return Container(
      color: colorLightGray,
      child: Icon(Icons.holiday_village, size: 80),
    );
  }
}
