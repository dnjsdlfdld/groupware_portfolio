import 'package:flutter/material.dart';

class ComponentTitleDesign extends StatelessWidget {
  const ComponentTitleDesign({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Text(
              title,
            style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18, letterSpacing: 1.5),
          )
        ],
      ),
    );
  }
}
