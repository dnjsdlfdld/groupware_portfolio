import 'package:flutter/material.dart';
import 'package:groupware_manager_front/model/attendance/all_attendance_list_item.dart';

class ComponentAttendanceItem extends StatelessWidget {
  const ComponentAttendanceItem(
      {super.key, required this.attendanceItem, required this.callback});

  final AllAttendanceListItem attendanceItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Column(
            children: [Text('사원명')],
          )),
    );
  }
}

// Text('상태'),
// Text('출근시간'),
// Text('퇴근시간'),`
