import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ComponentCupertino extends StatelessWidget {
  const ComponentCupertino({super.key, required this.cancelFunc});

  final CancelFunc cancelFunc;

  @override
  Widget build(BuildContext context) {
    return const CupertinoActivityIndicator(
      radius: 20,
    );
  }
}
