class MemberManageDetailItem {
  int id;
  String name;
  String birth;
  String phone;
  String address;
  String position;
  String department;
  String username;
  String password;
  bool isEnable;
  bool isAdmin;
  DateTime dateCreate;
  DateTime dateUpdate;
  String dateIn;
  String? dateOut;

  MemberManageDetailItem(
      this.id,
      this.name,
      this.birth,
      this.phone,
      this.address,
      this.position,
      this.department,
      this.username,
      this.password,
      this.isEnable,
      this.isAdmin,
      this.dateCreate,
      this.dateUpdate,
      this.dateIn,
      {this.dateOut}
      );

  factory MemberManageDetailItem.fromJson(Map<String, dynamic> json) {
    return MemberManageDetailItem(
      json['id'],
      json['name'],
      json['birth'],
      json['phone'],
      json['address'],
      json['position'],
      json['department'],
      json['username'],
      json['password'],
      json['isEnable'],
      json['isAdmin'],
      DateTime.parse(json['dateCreate']),
      DateTime.parse(json['dateUpdate']),
      json['dateIn'],
      dateOut: json['dateOut'] != null ? null : json['dateOut']
    );
  }
}