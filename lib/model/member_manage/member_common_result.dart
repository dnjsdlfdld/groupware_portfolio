class MemberCommonResult {
  bool isSuccess;
  int code;
  String msg;

  MemberCommonResult(
      this.isSuccess,
      this.code,
      this.msg,
      );

  factory MemberCommonResult.fromJson(Map<String, dynamic> json) {
    return MemberCommonResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}