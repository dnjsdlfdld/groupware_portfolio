import 'package:groupware_manager_front/model/member_manage/member_manage_item.dart';

class MemberManageListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<MemberManageItem>? list;

  MemberManageListResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      {this.list}
      );

  factory MemberManageListResult.fromJson(Map<String, dynamic> json) {
    return MemberManageListResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      list: json['list'] == null ? null : (json['list'] as List).map((e) => MemberManageItem.fromJson(e)).toList()
    );
  }
}