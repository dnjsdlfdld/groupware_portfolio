class MemberJoinRequest {
  String name;
  String birth;
  String phone;
  String address;
  String position;
  String department;
  String username;
  String password;
  String dateIn;

  MemberJoinRequest(
      this.name,
      this.birth,
      this.phone,
      this.address,
      this.position,
      this.department,
      this.username,
      this.password,
      this.dateIn
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['name'] = this.name;
    data['birth'] = this.birth;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['position'] = this.position;
    data['department'] = this.department;
    data['username'] = this.username;
    data['password'] = this.password;
    data['dateIn'] = this.dateIn;

    return data;
  }
}