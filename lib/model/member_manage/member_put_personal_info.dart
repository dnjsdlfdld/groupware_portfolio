class MemberPutPersonalInfo {
  String name;
  String phone;
  String address;

  MemberPutPersonalInfo(
      this.name,
      this.phone,
      this.address
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['address'] = this.address;

    return data;
  }
}