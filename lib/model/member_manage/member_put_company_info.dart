class MemberPutCompanyInfo {
  String department;
  String position;

  MemberPutCompanyInfo(
      this.department,
      this.position
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['department'] = this.department;
    data['position'] = this.position;

    return data;
  }
}