import 'package:groupware_manager_front/model/member_manage/member_manage_detail_item.dart';

class MemberManageDetailCommonResult {
  bool isSuccess;
  int code;
  String msg;
  MemberManageDetailItem? data;

  MemberManageDetailCommonResult(
      this.isSuccess,
      this.code,
      this.msg,
      {this.data}
      );

  factory MemberManageDetailCommonResult.fromJson(Map<String, dynamic> json) {
    return MemberManageDetailCommonResult(
      json['isSuccess'],
      json['code'],
      json['msg'],
      data: json['data'] != null ? MemberManageDetailItem.fromJson(json['data']) : null
    );
  }
}