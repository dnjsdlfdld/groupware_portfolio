class MemberManageItem {
  int id;
  String name;
  String dateIn;

  MemberManageItem(
      this.id,
      this.name,
      this.dateIn
      );

  factory MemberManageItem.fromJson(Map<String, dynamic> json) {
    return MemberManageItem(
      json['id'],
      json['name'],
      json['dateIn']
    );
  }
}