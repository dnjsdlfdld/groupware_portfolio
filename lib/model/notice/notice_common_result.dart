class NoticeCommonResult {
  bool isSuccess;
  int code;
  String msg;

  NoticeCommonResult(
      this.isSuccess,
      this.code,
      this.msg,
      );

  factory NoticeCommonResult.fromJson(Map<String, dynamic> json) {
    return NoticeCommonResult(
      json['isSuccess'],
      json['code'],
      json['msg'],
    );
  }
}