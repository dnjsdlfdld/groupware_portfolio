class NoticeHomeItem {
  String noticeTitle;

  NoticeHomeItem(
      this.noticeTitle
      );

  factory NoticeHomeItem.fromJason(Map<String, dynamic> json) {
    return NoticeHomeItem(
      json['noticeTitle']
    );
  }
}