import 'dart:convert';

import 'package:groupware_manager_front/model/notice/notice_home_item.dart';

class NoticeListResult {
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<NoticeHomeItem>? list;

  NoticeListResult(
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      {this.list}
      );

  factory NoticeListResult.fromJson(Map<String, dynamic> json) {
    return NoticeListResult(
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        list: json['list'] == null ? null : (json['list'] as List).map((e) => NoticeHomeItem.fromJason(e)).toList()
    );
  }
}