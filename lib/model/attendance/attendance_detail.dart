class AttendanceDetail {
   int memberId;
   String memberName;
   String dateWork;
   String attendanceType;
   String timeStart;
   String? timeLeave;
   String? timeEarlyLeave;

   AttendanceDetail(
       this.memberId,
       this.memberName,
       this.dateWork,
       this.attendanceType,
       this.timeStart,
   {this.timeLeave, this.timeEarlyLeave}
       );

   factory AttendanceDetail.fromJson(Map<String, dynamic> json) {
     return AttendanceDetail(
       json['memberId'],
       json['memberName'],
       json['dateWork'],
       json['attendanceType'],
       json['timeStart'],
       timeLeave: json['timeLeave'] == null ? "-" : json['timeLeave'],
       timeEarlyLeave: json['timeEarlyLeave'] == null ? "-" : json['timeEarlyLeave']
     );
   }
}