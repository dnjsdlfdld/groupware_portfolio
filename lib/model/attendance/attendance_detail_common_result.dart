import 'package:groupware_manager_front/model/attendance/attendance_detail.dart';

class AttendanceDetailCommonResult {
  bool isSuccess;
  int code;
  String msg;
  AttendanceDetail data;

  AttendanceDetailCommonResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.data
      );

  factory AttendanceDetailCommonResult.fromJson(Map<String, dynamic> json) {
    return AttendanceDetailCommonResult(
      json['isSuccess'],
      json['code'],
      json['msg'],
      AttendanceDetail.fromJson(json['data'])
    );
  }
}