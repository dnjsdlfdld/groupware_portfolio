class AllAttendanceListItem {
  int memberId;
  String memberName;
  String attendanceType;
  String timeStart;
  String? timeLeave;
  String? timeEarlyLeave;

  AllAttendanceListItem(
      this.memberId,
      this.memberName,
      this.attendanceType,
      this.timeStart,
      {this.timeLeave, this.timeEarlyLeave});

  factory AllAttendanceListItem.fromJson(Map<String, dynamic> json) {
    return AllAttendanceListItem(
      json['memberId'],
      json['memberName'],
      json['attendanceType'],
      json['timeStart'],
      timeLeave: json['timeLeave'] == null ? "-" : json['timeLeave'],
      timeEarlyLeave: json['timeEarlyLeave'] == null ? "-" : json['timeEarlyLeave']
    );
  }
}