import 'package:groupware_manager_front/model/attendance/all_attendance_list_item.dart';

class AllAttendanceListResult{
   bool isSuccess;
   int code;
   String msg;
   int totalItemCount;
   int totalPage;
   int currentPage;
   List<AllAttendanceListItem>? list;

   AllAttendanceListResult(
       this.isSuccess,
       this.code,
       this.msg,
       this.totalItemCount,
       this.totalPage,
       this.currentPage,
       {this.list}
       );

   factory AllAttendanceListResult.fromJson(Map<String, dynamic> json) {
     return AllAttendanceListResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        list: json['list'] == null ? [] : (json['list'] as List).map((e) => AllAttendanceListItem.fromJson(e)).toList()
     );
   }
}