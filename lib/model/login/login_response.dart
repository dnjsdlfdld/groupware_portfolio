class LoginResponse {
  int memberId;
  String name;
  String loginTime;

  LoginResponse(
      this.memberId,
      this.name,
      this.loginTime
      );

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
      json['memberId'],
      json['name'],
      json['loginTime']
    );
  }
}