import 'package:groupware_manager_front/model/login/login_response.dart';

class LoginCommonResult {
  bool isSuccess;
  int code;
  String msg;
  LoginResponse data;

  LoginCommonResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.data
      );

  factory LoginCommonResult.fromJson(Map<String, dynamic> json) {
    return LoginCommonResult(

      json['isSuccess'],
      json['code'],
      json['msg'],
      LoginResponse.fromJson(json['data'])
    );
  }
}