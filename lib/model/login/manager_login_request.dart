class ManagerLoginRequest {
   String username;
   String password;
   int managerPin;

   ManagerLoginRequest(
       this.username,
       this.password,
       this.managerPin
       );

   Map<String, dynamic> toJson() {
     final Map<String, dynamic> data = Map<String, dynamic>();
     data['username'] = this.username;
     data['password'] = this.password;
     data['managerPin'] = this.managerPin;

     return data;
   }

}