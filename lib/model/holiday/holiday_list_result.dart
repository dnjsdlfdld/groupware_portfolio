import 'package:groupware_manager_front/model/holiday/holiday_list.dart';

class HolidayListResult {
   bool isSuccess;
   int code;
   String msg;
   int totalItemCount;
   int totalPage;
   int currentPage;
   List<HolidayList>? list;

   HolidayListResult(
       this.isSuccess,
       this.code,
       this.msg,
       this.totalItemCount,
       this.totalPage,
       this.currentPage,
       {this.list}
       );

   factory HolidayListResult.fromJson(Map<String, dynamic> json) {
     return HolidayListResult(
       json['isSuccess'],
       json['code'],
       json['msg'],
       json['totalItemCount'],
       json['totalPage'],
       json['currentPage'],
       list: json['list'] == null ? [] : (json['list'] as List).map((e) => HolidayList.fromJson(e)).toList()
     );
   }
}