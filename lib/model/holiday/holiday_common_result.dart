import 'package:groupware_manager_front/model/holiday/holiday_detail.dart';

class HolidayCommonResult {
   bool isSuccess;
   int code;
   String msg;
   HolidayDetail data;

   HolidayCommonResult(
       this.isSuccess,
       this.code,
       this.msg,
       this.data
       );

   factory HolidayCommonResult.fromJson(Map<String, dynamic> json) {
     return HolidayCommonResult(
       json['isSuccess'],
       json['code'],
       json['msg'],
       HolidayDetail.fromJson(json['data'])
     );
   }
}