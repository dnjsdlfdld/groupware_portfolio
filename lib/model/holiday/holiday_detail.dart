class HolidayDetail {
   int uploadId;
   String memberFullName;
   String title;
   String content;
   String holidayType;
   DateTime startHoliday;
   DateTime endHoliday;
   String useHoliday;
   String approvalState;
   String approvalDate;
   String companionDate;

   HolidayDetail(
       this.uploadId,
       this.memberFullName,
       this.title,
       this.content,
       this.holidayType,
       this.startHoliday,
       this.endHoliday,
       this.useHoliday,
       this.approvalState,
       this.approvalDate,
       this.companionDate
       );

   factory HolidayDetail.fromJson(Map<String, dynamic> json) {
     return HolidayDetail(
       json['uploadId'],
       json['memberFullName'],
       json['title'],
       json['content'],
       json['holidayType'],
       DateTime.parse(json['startHoliday']),
       DateTime.parse(json['endHoliday']),
       json['useHoliday'],
       json['approvalState'],
       json['approvalDate'],
       json['companionDate'],
     );
   }
}

// companionDate: json['companionDate'] == null ? null :