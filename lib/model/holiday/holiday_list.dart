class HolidayList {
   int uploadId;
   String memberFullName;
   String title;

   HolidayList(
       this.uploadId,
       this.memberFullName,
       this.title
       );

   factory HolidayList.fromJson(Map<String, dynamic> json) {
     return HolidayList(
       json['uploadId'],
       json['memberFullName'],
       json['title']
     );
   }
}