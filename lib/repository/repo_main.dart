import 'package:dio/dio.dart';
import 'package:groupware_manager_front/config/config_api.dart';
import 'package:groupware_manager_front/model/notice/notice_list_result.dart';

class RepoMain {
  Future<NoticeListResult> getNotice() async {
    const String baseUrl = '$apiUri/notice/top';

    Dio dio = Dio();

    final response = await dio.get(baseUrl,
        options: Options(
          followRedirects: false,
          validateStatus: (status) => (status == 200),
        ));

    return NoticeListResult.fromJson(response.data);
  }
}
