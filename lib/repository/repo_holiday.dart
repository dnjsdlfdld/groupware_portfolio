import 'package:dio/dio.dart';
import 'package:groupware_manager_front/config/config_api.dart';
import 'package:groupware_manager_front/model/holiday/holiday_common_result.dart';
import 'package:groupware_manager_front/model/holiday/holiday_list_result.dart';

class RepoHoliday {
  Future<HolidayListResult> getList() async {
    const baseUrl = '$apiUri/holiday/list-simple';

    Dio dio = Dio();

    final response = await dio.get(baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return HolidayListResult.fromJson(response.data);
  }

  Future<HolidayCommonResult> getDetailList(int uploadId) async {
    const baseUrl = '$apiUri/holiday/detail/{uploadId}';

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl.replaceAll('{uploadId}', uploadId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return HolidayCommonResult.fromJson(response.data);
  }
}
