import 'package:groupware_manager_front/config/config_api.dart';
import 'package:groupware_manager_front/model/login/login_common_result.dart';
import 'package:dio/dio.dart';
import 'package:groupware_manager_front/model/login/manager_login_request.dart';
import 'package:groupware_manager_front/model/member_manage/member_common_result.dart';
import 'package:groupware_manager_front/model/member_manage/member_join_request.dart';
import 'package:groupware_manager_front/model/member_manage/member_manage_detail_common_result.dart';
import 'package:groupware_manager_front/model/member_manage/member_manage_list_result.dart';
import 'package:groupware_manager_front/model/member_manage/member_put_company_info.dart';
import 'package:groupware_manager_front/model/member_manage/member_put_personal_info.dart';

class RepoMember {
  /// 로그인
  Future<LoginCommonResult> doLogin(ManagerLoginRequest request) async {
    const String baseUrl = '$apiUri/login/manager';

    Dio dio = Dio();
    final response = await dio.post(
      baseUrl,
      data: request.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    return LoginCommonResult.fromJson(response.data);
  }

  /// 직원 간략정보 가져오기
  Future<MemberManageListResult> getMembers() async {
    const String baseUrl = '$apiUri/member/members/all';

    Dio dio = Dio();

    final response = await dio.get(baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));

    return MemberManageListResult.fromJson(response.data);
  }

  /// 직원 상세정보 가져오기
  Future<MemberManageDetailCommonResult> getMemberDetail(int memberId) async {
    const String baseUrl = '$apiUri/member/detail/{memberId}';

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', memberId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));

    return MemberManageDetailCommonResult.fromJson(response.data);
  }

  /// 직원 등록
  Future<MemberCommonResult> setMemberJoin(MemberJoinRequest request) async {
    const String baseUrl = '$apiUri/member/new-join';

    Dio dio = Dio();

    final response = await dio.post(baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));

    return MemberCommonResult.fromJson(response.data);
  }

  /// 사원의 개인정보 수정
  Future<MemberCommonResult> putMemberPersonal(
      int memberId, MemberPutPersonalInfo putPersonalInfo) async {
    const String baseUrl = '$apiUri/member/personal-info/{memberId}';

    Dio dio = Dio();

    final response = await dio.put(
        baseUrl.replaceAll('{memberId}', memberId.toString()),
        data: putPersonalInfo.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));

    return MemberCommonResult.fromJson(response.data);
  }

  /// 사원의 인사정보 수정
  Future<MemberCommonResult> putMemberCompany(
      int memberId, MemberPutCompanyInfo companyInfo) async {
    const String baseUrl = '$apiUri/member/company-info/{memberId}';

    Dio dio = Dio();

    final response = await dio.put(
        baseUrl.replaceAll('{memberId}', memberId.toString()),
        data: companyInfo.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));

    return MemberCommonResult.fromJson(response.data);
  }

  /// 퇴사처리
  Future<MemberCommonResult> delMember(int memberId) async {
    const String baseUrl = '$apiUri/member/del-id/{memberId}';

    Dio dio = Dio();

    final response = await dio.delete(
        baseUrl.replaceAll('{memberId}', memberId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return MemberCommonResult.fromJson(response.data);
  }
}
