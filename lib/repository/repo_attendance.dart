import 'package:dio/dio.dart';
import 'package:groupware_manager_front/config/config_api.dart';
import 'package:groupware_manager_front/model/attendance/all_attendance_list_result.dart';
import 'package:groupware_manager_front/model/attendance/attendance_detail_common_result.dart';

class RepoAttendance {
  Future<AllAttendanceListResult> getAttendance() async {
    const String baseUrl = '$apiUri/attendance/attendance-list';

    Dio dio = Dio();

    final response = await dio.get(baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));

    return AllAttendanceListResult.fromJson(response.data);
  }

  Future<AttendanceDetailCommonResult> getAttendanceDetail(int memberId) async {
    const String baseUrl = '$apiUri/attendance/detail/{memberId}';

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', memberId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));

    return AttendanceDetailCommonResult.fromJson(response.data);
  }
}
