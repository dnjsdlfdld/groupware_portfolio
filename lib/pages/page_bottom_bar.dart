import 'package:flutter/material.dart';
import 'package:groupware_manager_front/pages/page_attendance_main.dart';
import 'package:groupware_manager_front/pages/page_holiday_list.dart';
import 'package:groupware_manager_front/pages/page_member_list.dart';
import 'package:groupware_manager_front/pages/page_home.dart';

class PageBottomBar extends StatefulWidget {
  const PageBottomBar({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PageBottomBarState();
}

class _PageBottomBarState extends State<PageBottomBar> {
  int _currentTabIndex = 0;

  @override
  Widget build(BuildContext context) {
    final _kTabPages = <Widget>[
      const PageHome(),
      const PageAttendanceMain(),
      const PageHolidayList(),
      const PageMemberList()
    ];
    final _kBottmonNavBarItems = <BottomNavigationBarItem>[
      const BottomNavigationBarItem(
          icon: Icon(Icons.home_outlined, color: Colors.white), label: '홈'),
      const BottomNavigationBarItem(
          icon: Icon(Icons.alarm, color: Colors.white), label: '근태관리'),
      const BottomNavigationBarItem(
          icon: Icon(Icons.trip_origin, color: Colors.white), label: '휴가관리'),
      const BottomNavigationBarItem(
          icon: Icon(Icons.person_outline, color: Colors.white), label: '사원관리')
    ];
    assert(_kTabPages.length == _kBottmonNavBarItems.length);
    final bottomNavBar = BottomNavigationBar(
      backgroundColor: Colors.black26,
      elevation: 0,
      items: _kBottmonNavBarItems,
      currentIndex: _currentTabIndex,
      type: BottomNavigationBarType.fixed,
      onTap: (int index) {
        setState(() {
          _currentTabIndex = index;
        });
      },
    );
    return Scaffold(
      body: _kTabPages[_currentTabIndex],
      bottomNavigationBar: bottomNavBar,
    );
  }
}
