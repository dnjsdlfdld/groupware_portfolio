import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:groupware_manager_front/components/component_cupertino.dart';
import 'package:groupware_manager_front/components/component_notification.dart';
import 'package:groupware_manager_front/config/config_dropdown.dart';
import 'package:groupware_manager_front/config/config_form_validator.dart';
import 'package:groupware_manager_front/model/member_manage/member_join_request.dart';
import 'package:groupware_manager_front/repository/repo_member.dart';
import 'package:intl/intl.dart';

class PageMemberSetForm extends StatefulWidget {
  PageMemberSetForm({super.key});

  @override
  State<PageMemberSetForm> createState() => _PageMemberSetFormState();
}

class _PageMemberSetFormState extends State<PageMemberSetForm> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setMember(MemberJoinRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoMember().setMemberJoin(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '사원이 등록되었습니다.',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();

      Navigator.pop(context, [true]);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '등록 실패',
        subTitle: '사원 등록에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: const Icon(Icons.close_sharp, color: Colors.black)),
        ],
      ),
      body: _buildBody(),
      backgroundColor: Colors.white,
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: FormBuilder(
        key: _formKey,
        child: Container(
          margin: const EdgeInsets.fromLTRB(30, 0, 30, 0),
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(30, 0, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset('assets/kakao2.png', width: 150, height: 150),
                    Image.asset('assets/chun.jpg', width: 150, height: 150)
                  ],
                ),
              ),
              FormBuilderTextField(
                textInputAction: TextInputAction.next,
                name: 'name',
                decoration: const InputDecoration(labelText: "이름"),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(20,
                      errorText: formErrorMaxLength(20)),
                ]),
                keyboardType: TextInputType.text,
              ),
              FormBuilderDateTimePicker(
                textInputAction: TextInputAction.next,
                name: 'birth',
                format: DateFormat('yyyy-MM-dd'),
                inputType: InputType.date,
                decoration: InputDecoration(
                  labelText: "생년월일",
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.close),
                    onPressed: () {
                      _formKey.currentState!.fields['birth']?.didChange(null);
                    },
                  ),
                ),
                locale: const Locale.fromSubtags(languageCode: 'ko'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired)
                ]),
              ),
              FormBuilderTextField(
                textInputAction: TextInputAction.next,
                name: 'phone',
                decoration: const InputDecoration(
                    labelText: "연락처",
                    helperText: 'ex) 010-1234-1234',
                    hintText: '"-" 하이픈을 넣어주세요'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(20,
                      errorText: formErrorMaxLength(20)),
                ]),
                keyboardType: TextInputType.number,
              ),
              FormBuilderTextField(
                textInputAction: TextInputAction.next,
                name: 'address',
                decoration: const InputDecoration(labelText: "주소"),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(50,
                      errorText: formErrorMaxLength(50)),
                ]),
                keyboardType: TextInputType.text,
              ),
              FormBuilderDropdown<String>(
                name: 'position',
                decoration: const InputDecoration(
                  labelText: '직급',
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired)
                ]),
                items: dropdownPosition,
              ),
              FormBuilderTextField(
                textInputAction: TextInputAction.next,
                name: 'department',
                decoration: const InputDecoration(labelText: "부서명"),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(20,
                      errorText: formErrorMaxLength(20)),
                ]),
                keyboardType: TextInputType.text,
              ),
              FormBuilderTextField(
                textInputAction: TextInputAction.next,
                name: 'username',
                decoration: const InputDecoration(labelText: "아이디"),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(20,
                      errorText: formErrorMaxLength(20)),
                ]),
                keyboardType: TextInputType.text,
              ),
              FormBuilderTextField(
                textInputAction: TextInputAction.next,
                name: 'password',
                decoration: const InputDecoration(labelText: "비밀번호"),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(20,
                      errorText: formErrorMaxLength(20)),
                ]),
                keyboardType: TextInputType.text,
              ),
              FormBuilderTextField(
                name: 'dateIn',
                decoration: const InputDecoration(labelText: "입사일"),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2,
                      errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(20,
                      errorText: formErrorMaxLength(20)),
                ]),
                keyboardType: TextInputType.text,
              ),
              const SizedBox(height: 15),
              Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                IconButton(
                  onPressed: () {
                    if (_formKey.currentState?.saveAndValidate() ?? false) {
                      MemberJoinRequest request = MemberJoinRequest(
                        _formKey.currentState!.fields['name']!.value,
                        DateFormat('yyyy-MM-dd').format(
                            _formKey.currentState!.fields['birth']!.value),
                        _formKey.currentState!.fields['phone']!.value,
                        _formKey.currentState!.fields['address']!.value,
                        _formKey.currentState!.fields['position']!.value,
                        _formKey.currentState!.fields['department']!.value,
                        _formKey.currentState!.fields['username']!.value,
                        _formKey.currentState!.fields['password']!.value,
                        _formKey.currentState!.fields['dateIn']!.value,
                      );

                      _setMember(request);
                    }
                  },
                  icon: const Icon(Icons.arrow_forward_ios_rounded),
                  tooltip: '버튼을 눌러 사원 등록을 완료하세요!',
                ),
              ])
            ],
          ),
        ),
      ),
    );
  }
}
