import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:groupware_manager_front/components/component_cupertino.dart';
import 'package:groupware_manager_front/components/component_notification.dart';
import 'package:groupware_manager_front/config/config_form_validator.dart';
import 'package:groupware_manager_front/functions/token_lib.dart';
import 'package:groupware_manager_front/middleware_login_check/middleware_login_check.dart';
import 'package:groupware_manager_front/model/login/manager_login_request.dart';
import 'package:groupware_manager_front/repository/repo_member.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(ManagerLoginRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoMember().doLogin(request).then((result) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '로그인 성공',
        subTitle: '로그인에 성공하였습니다.',
      ).call();

      TokenLib.setToken(result.data.memberId.toString());
      MiddlewareLoginCheck().check(context);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '로그인 실패',
        subTitle: '아이디 혹은 비밀번호를 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  margin: const EdgeInsets.only(top: 100),
                  child: Image.asset('assets/kakaoLogo.png',
                      width: 200, height: 100)),
            ],
          ),
          const SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  width: 200,
                  height: 25,
                  decoration: const BoxDecoration(
                      border: Border(
                    top: BorderSide(color: Colors.amber),
                    bottom: BorderSide(color: Colors.amber),
                    left: BorderSide(color: Colors.amber),
                    right: BorderSide(color: Colors.amber),
                  )),
                  child: const Text('관리자 / 인사팀',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          height: 1.4, letterSpacing: 1.2, fontSize: 15))),
            ],
          ),
          FormBuilder(
              key: _formKey,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              child: Column(
                children: [
                  Container(
                      margin: const EdgeInsets.fromLTRB(40, 0, 40, 0),
                      child: FormBuilderTextField(
                        textInputAction: TextInputAction.next,
                        style: const TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 15),
                        textAlign: TextAlign.center,
                        name: 'username',
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(
                              errorText: formErrorRequired),
                          FormBuilderValidators.minLength(5,
                              errorText: formErrorMinLength(5)),
                          FormBuilderValidators.maxLength(20,
                              errorText: formErrorMaxLength(20)),
                        ]),
                        decoration: const InputDecoration(
                            labelText: 'ID',
                            labelStyle: TextStyle(fontSize: 13),
                            hintText: '아이디를 입력해주세요.',
                            hintStyle: TextStyle(fontSize: 13)),
                        keyboardType: TextInputType.text,
                      )),
                  Container(
                      margin: const EdgeInsets.fromLTRB(40, 0, 40, 0),
                      child: FormBuilderTextField(
                        textInputAction: TextInputAction.next,
                        style: const TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 15),
                        textAlign: TextAlign.center,
                        name: 'password',
                        obscureText: true,
                        obscuringCharacter: '●',
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(
                              errorText: formErrorRequired),
                          FormBuilderValidators.minLength(8,
                              errorText: formErrorMinLength(8)),
                          FormBuilderValidators.maxLength(20,
                              errorText: formErrorMaxLength(20)),
                        ]),
                        decoration: const InputDecoration(
                            labelText: 'PASSWORD',
                            labelStyle: TextStyle(fontSize: 13),
                            hintText: '비밀번호를 입력해주세요',
                            hintStyle: TextStyle(fontSize: 13)),
                        keyboardType: TextInputType.text,
                      )),
                  Container(
                      margin: const EdgeInsets.fromLTRB(40, 0, 40, 0),
                      child: FormBuilderTextField(
                        textInputAction: TextInputAction.next,
                        style: const TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 15),
                        textAlign: TextAlign.center,
                        name: 'managerPin',
                        obscureText: true,
                        obscuringCharacter: '●',
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(
                              errorText: formErrorRequired),
                          FormBuilderValidators.minLength(4,
                              errorText: formErrorMinLength(4)),
                          FormBuilderValidators.maxLength(4,
                              errorText: formErrorMaxLength(4)),
                        ]),
                        decoration: const InputDecoration(
                            labelText: 'OTP',
                            labelStyle: TextStyle(fontSize: 13),
                            hintText: '발급된 4자리를 입력해주세요',
                            hintStyle: TextStyle(fontSize: 13)),
                        keyboardType: TextInputType.number,
                      )),
                  const SizedBox(height: 20),
                  Container(
                      width: 335,
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                            backgroundColor: Colors.amber),
                        onPressed: () {
                          if (_formKey.currentState?.saveAndValidate() ??
                              false) {
                            ManagerLoginRequest request = ManagerLoginRequest(
                              _formKey.currentState!.fields['username']!.value,
                              _formKey.currentState!.fields['password']!.value,
                              int.parse(_formKey
                                  .currentState!.fields['managerPin']!.value),
                            );
                            _doLogin(request);
                          }
                        },
                        child: const Text('로그인',
                            style: TextStyle(color: Colors.white)),
                      )),
                  Image.asset('assets/friend.jpg', width: 400, height: 400)
                ],
              ))
        ],
      ),
    );
  }
}
