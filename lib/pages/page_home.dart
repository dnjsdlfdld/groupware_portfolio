import 'package:banner_carousel/banner_carousel.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:groupware_manager_front/components/component_cupertino.dart';
import 'package:groupware_manager_front/components/component_notice_home_item.dart';
import 'package:groupware_manager_front/components/component_title_design.dart';
import 'package:groupware_manager_front/functions/token_lib.dart';
import 'package:groupware_manager_front/model/notice/notice_home_item.dart';
import 'package:groupware_manager_front/repository/repo_main.dart';
import 'package:table_calendar/table_calendar.dart';

class PageHome extends StatefulWidget {
  const PageHome({Key? key}) : super(key: key);

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  late final bool Function(DateTime day)? selectedDayPredicate;

  DateTime selectedDay =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);

  DateTime focusedDay = DateTime.now();

  List<NoticeHomeItem> _list = [];

  @override
  void initState() {
    super.initState();
    _loadItem();
  }

  Future<void> _loadItem() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoMain().getNotice().then((result) {
      BotToast.closeAllLoading();

      setState(() {
        _list = result.list!;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();
    });
  }

  Future<void> _logout(BuildContext context) async {
    // functions/token_lib.dart 에서 만든 로그아웃 기능 호출
    TokenLib.logout(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/chun1.png', width: 100, height: 100),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
                child: IconButton(
                  onPressed: () {
                    _logout(context);
                  },
                  icon: const Icon(Icons.logout, color: Colors.black),
                  tooltip: '로그아웃',
                ),
              )
            ],
          ),
          Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
              child: const ComponentTitleDesign(title: 'KaKao 소식')),
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              BannerCarousel(
                banners: BannerImages.listBanners,
                customizedIndicators: const IndicatorModel.animation(
                    width: 20, height: 5, spaceBetween: 2, widthAnimation: 50),
                height: 180,
                activeColor: Colors.amberAccent,
                disableColor: Colors.white,
                animation: true,
                borderRadius: 10,
                onTap: (id) => print(id),
                width: 370,
                indicatorBottom: false,
              )
            ],
          ),
          const SizedBox(height: 30),
          Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
              child: const ComponentTitleDesign(title: '공지사항')),
          const SizedBox(height: 20),
          _buildNotice(),
          const SizedBox(height: 40),
          Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
              child: const ComponentTitleDesign(title: 'KaKao 일정')),
          const SizedBox(height: 15),
          Container(
            margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: TableCalendar(
              locale: 'ko_KR',
              focusedDay: focusedDay,
              firstDay: DateTime.utc(1900, 01, 01),
              lastDay: DateTime.utc(2100, 01, 01),
              onDaySelected: (DateTime selectedDay, DateTime focusedDay) {
                setState(() {
                  this.selectedDay = selectedDay;
                  this.focusedDay = focusedDay;
                });
              },
              selectedDayPredicate: (DateTime day) {
                return isSameDay(selectedDay, day);
              },
              headerStyle: const HeaderStyle(
                titleCentered: true,
                //titleTextFormatter: (date, locale) =>
                //DateFormat.yMMMMd(locale).format(date),
                formatButtonVisible: false,
                titleTextStyle: TextStyle(fontSize: 20.0, color: Colors.amber),
              ),
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
    );
  }

  Widget _buildNotice() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) =>
                  ComponentNoticeHomeItem(callback: () {}, item: _list[index])),
        ],
      ),
    );
  }
}

class BannerImages {
  static const String banner1 = 'assets/kakaoBank.png';
  static const String banner2 = 'assets/kakao4.jpg';
  static const String banner3 = 'assets/kakaoMo.jpg';
  static const String banner4 = 'assets/kakaoPay.jpg';

  static List<BannerModel> listBanners = [
    BannerModel(imagePath: banner1, id: "1"),
    BannerModel(imagePath: banner2, id: "2"),
    BannerModel(imagePath: banner3, id: "3"),
    BannerModel(imagePath: banner4, id: "4"),
  ];
}

typedef OnDaySelected = void Function(
    DateTime selectedDay, DateTime focusedDay);
