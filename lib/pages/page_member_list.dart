import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:groupware_manager_front/components/component_count_title.dart';
import 'package:groupware_manager_front/components/component_cupertino.dart';
import 'package:groupware_manager_front/components/component_member_item.dart';
import 'package:groupware_manager_front/components/component_notification.dart';
import 'package:groupware_manager_front/components/component_refresh_button.dart';
import 'package:groupware_manager_front/config/config_dropdown.dart';
import 'package:groupware_manager_front/model/member_manage/member_manage_item.dart';
import 'package:groupware_manager_front/pages/page_member_detail.dart';
import 'package:groupware_manager_front/pages/page_member_set_form.dart';
import 'package:groupware_manager_front/repository/repo_member.dart';

class PageMemberList extends StatefulWidget {
  const PageMemberList({Key? key}) : super(key: key);

  @override
  State<PageMemberList> createState() => _PageMemberListState();
}

class _PageMemberListState extends State<PageMemberList> {
  final _scrollController = ScrollController();
  final _formKey = GlobalKey<FormBuilderState>();
  bool _selected = true;

  List<MemberManageItem> _list = [];
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();

    _getList();
  }

  Future<void> _getList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoMember().getMembers().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list!;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        controller: _scrollController,
        children: [
          Container(
            margin: const EdgeInsets.fromLTRB(15, 20, 15, 0),
            child: Image.asset('assets/friend2.png', width: 400, height: 100),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                    margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                    child: ComponentCountTitle(
                        icon: Icons.person_outline,
                        count: _totalItemCount,
                        unitName: '명',
                        itemName: '크루'),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(80, 0, 0, 0),
                    child: IconButton(
                      padding: EdgeInsets.zero,
                      onPressed: () async {
                        await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PageMemberSetForm()))
                            .then((value) => {
                                  setState(() {
                                    _getList();
                                  })
                                });
                      },
                      icon: const Icon(Icons.add),
                      tooltip: '직원 등록하기',
                    ),
                  ),
                  ComponentRefreshButton(
                    action1Callback: () {
                      _getList();
                    },
                    action1Icon: Icons.refresh,
                  )
                ],
              ),
            ],
          ),
          _buildCheck(),
          const SizedBox(height: 30),
          _buildBody(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [],
          ),
        ],
      ),
      backgroundColor: Colors.white,
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentMemberItem(
                manageItem: _list[index],
                callback: () async {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              PageMemberDetail(memberId: _list[index].id)));
                  _getList();
                })),
      ],
    );
  }

  Widget _buildCheck() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(20, 0, 300, 0),
            child: FormBuilder(
              key: _formKey,
              child: FormBuilderDropdown<bool>(
                name: 'isEnable',
                decoration: InputDecoration(labelText: '구분'),
                initialValue: _selected,
                onChanged: (val) {
                  setState(() {
                    _selected = val!;
                  });
                  _getList();
                },
                items: dropdownIsEnable,
              ),
            ),
          )
        ],
      ),
    );
  }
}
