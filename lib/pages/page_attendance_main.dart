import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:groupware_manager_front/components/component_attendance_list.dart';
import 'package:groupware_manager_front/components/component_count_title.dart';
import 'package:groupware_manager_front/components/component_cupertino.dart';
import 'package:groupware_manager_front/components/component_notification.dart';
import 'package:groupware_manager_front/components/component_refresh_button.dart';
import 'package:groupware_manager_front/model/attendance/all_attendance_list_item.dart';
import 'package:groupware_manager_front/pages/page_attendance_detail.dart';
import 'package:groupware_manager_front/repository/repo_attendance.dart';
import 'package:intl/intl.dart';

class PageAttendanceMain extends StatefulWidget {
  const PageAttendanceMain({
    super.key,
  });

  @override
  State<PageAttendanceMain> createState() => _PageAttendanceMainState();
}

class _PageAttendanceMainState extends State<PageAttendanceMain> {
  String getToday() {
    DateTime _today = DateTime.now();
    DateFormat format = DateFormat('yyyy-MM-dd');
    var strToday = format.format(_today);
    return strToday;
  }

  List<AllAttendanceListItem> _list = [];
  int _totalCount = 0;

  @override
  void initState() {
    super.initState();

    _loadItem();
  }

  Future<void> _loadItem() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoAttendance().getAttendance().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list!;
        _totalCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            height: 150,
            margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: Image.asset('assets/chun3.png'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: ComponentCountTitle(
                    icon: Icons.document_scanner_outlined,
                    count: _totalCount,
                    unitName: '명',
                    itemName: '출근'),
              ),
              ComponentRefreshButton(
                action1Callback: () {
                  _loadItem();
                },
                action1Icon: Icons.refresh,
              )
            ],
          ),
          _buildBody()
        ],
      ),
      backgroundColor: Colors.white,
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
        child: Container(
      child: Column(
        children: [
          SizedBox(height: 20),
          ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) => ComponentAttendanceList(
                    attendanceListItem: _list[index],
                    callback: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PageAttendanceDetail(
                                  memberId: _list[index].memberId)));
                      _loadItem();
                    },
                  )),
        ],
      ),
    ));
  }
}
