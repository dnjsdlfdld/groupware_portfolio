import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:groupware_manager_front/components/component_count_title.dart';
import 'package:groupware_manager_front/components/component_cupertino.dart';
import 'package:groupware_manager_front/components/component_holiday_list.dart';
import 'package:groupware_manager_front/components/component_notification.dart';
import 'package:groupware_manager_front/components/component_refresh_button.dart';
import 'package:groupware_manager_front/model/holiday/holiday_list.dart';
import 'package:groupware_manager_front/pages/page_holiday_detail.dart';
import 'package:groupware_manager_front/repository/repo_holiday.dart';

class PageHolidayList extends StatefulWidget {
  const PageHolidayList({Key? key}) : super(key: key);

  @override
  State<PageHolidayList> createState() => _PageHolidayListState();
}

class _PageHolidayListState extends State<PageHolidayList> {
  List<HolidayList> _list = [];
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();

    _getList();
  }

  Future<void> _getList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoHoliday().getList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list!;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      children: [
        Container(
          height: 230,
          margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
          child: Image.asset('assets/kakaofriends.jpg'),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
              child: ComponentCountTitle(
                  icon: Icons.document_scanner_outlined,
                  count: _totalItemCount,
                  unitName: '건',
                  itemName: '휴가'),
            ),
            ComponentRefreshButton(
              action1Callback: () {
                _getList();
              },
              action1Icon: Icons.refresh,
            )
          ],
        ),
        _buildBody()
      ],
    ));
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            SizedBox(height: 10),
            ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: _list.length,
                itemBuilder: (_, index) => ComponentHolidayList(
                    holidayList: _list[index],
                    callback: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PageHolidayDetail(
                                  uploadId: _list[index].uploadId)));
                      _getList();
                    })),
          ],
        ),
      ),
    );
  }
}
