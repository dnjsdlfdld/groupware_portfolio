import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:groupware_manager_front/components/component_cupertino.dart';
import 'package:groupware_manager_front/components/component_notification.dart';
import 'package:groupware_manager_front/components/component_refresh_button.dart';
import 'package:groupware_manager_front/components/component_title_design.dart';
import 'package:groupware_manager_front/model/member_manage/member_manage_detail_item.dart';
import 'package:groupware_manager_front/pages/page_member_put_company_form.dart';
import 'package:groupware_manager_front/pages/page_member_put_personal_form.dart';
import 'package:groupware_manager_front/repository/repo_member.dart';
import 'package:intl/intl.dart';

class PageMemberDetail extends StatefulWidget {
  PageMemberDetail({super.key, required this.memberId});

  final int memberId;
  MemberManageDetailItem? memberManageDetailItem;

  @override
  State<PageMemberDetail> createState() => _PageMemberDetailState();
}

class _PageMemberDetailState extends State<PageMemberDetail> {
  final DateFormat dFormat = DateFormat('yyyy-MM-dd');
  final DateFormat dateFormat = DateFormat('yyyy-MM-dd HH:mm:ss');

  MemberManageDetailItem _item = MemberManageDetailItem(0, '', '', '', '', '',
      '', '', '', false, false, DateTime.now(), DateTime.now(), '');

  @override
  void initState() {
    super.initState();
    _getDetail();
  }

  Future<void> _delMember() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoMember().delMember(widget.memberId).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '퇴사 처리되었습니다.',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '처리 실패',
        subTitle: '네트워크가 원활하지 않습니다. \n다시한번 시도해주세요.',
      ).call();

      Navigator.pop(context);
    });
  }

  Future<void> _getDetail() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoMember().getMemberDetail(widget.memberId).then((result) {
      BotToast.closeAllLoading();

      setState(() {
        _item = result.data!;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        elevation: 0,
        actions: [
          IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: const Icon(Icons.close_sharp, color: Colors.black)),
        ],
      ),
      body: _buildBody(),
      backgroundColor: Colors.white,
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        Container(
          margin: const EdgeInsets.fromLTRB(24, 45, 0, 20),
          child: Row(
            children: [
              ComponentTitleDesign(title: '${_item.name}님의 상세정보'),
              ComponentRefreshButton(
                action1Callback: () {
                  _getDetail();
                },
                action1Icon: Icons.refresh_outlined,
              )
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('등록번호'),
            trailing: Text('${_item.id}'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('이름'),
            trailing: Text(_item.name),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('생년월일'),
            trailing: Text(_item.birth),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('연락처'),
            trailing: Text(_item.phone),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('주소'),
            trailing: Text(_item.address),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('직급'),
            trailing: Text(_item.position),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('부서'),
            trailing: Text(_item.department),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('아이디'),
            trailing: Text(_item.username),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: const ListTile(
            title: Text('비밀번호'),
            trailing: Text('(본인만 확인 가능)'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('재직여부'),
            trailing: Text(_item.isEnable == true ? '재직중' : '퇴사'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('권한'),
            trailing: Text(_item.isAdmin == false ? '사원' : '관리자'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('등록일자'),
            trailing: Text(dFormat.format(_item.dateCreate)),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('수정시간'),
            trailing: Text(dateFormat.format(_item.dateUpdate)),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('입사일자'),
            trailing: Text(_item.dateIn),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('퇴사일자'),
            trailing: Text(_item.dateOut ?? '-'),
          ),
        ),
        const SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: TextButton(
                onPressed: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PageMemberPutPersonalForm(
                              memberManageDetailItem: _item))).then((value) => {
                        setState(() {
                          _getDetail();
                        })
                      });
                },
                style: TextButton.styleFrom(
                  primary: Colors.black,
                ),
                child: const Text('개인정보수정'),
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: TextButton(
                onPressed: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PageMemberPutCompanyForm(
                              memberManageDetailItem: _item))).then((value) => {
                        setState(() {
                          _getDetail();
                        })
                      });
                },
                style: TextButton.styleFrom(
                  primary: Colors.black,
                ),
                child: const Text('인사정보수정'),
              ),
            )
          ],
        ),
        Container(
            margin: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  onPressed: () async {
                    _showDeleteDialog();
                  },
                  style: TextButton.styleFrom(
                    primary: Colors.black,
                  ),
                  child: const Text('퇴사처리',
                      style: TextStyle(
                          color: Colors.grey,
                          decoration: TextDecoration.underline,
                          decorationStyle: TextDecorationStyle.solid,
                          decorationThickness: 5)),
                ),
              ],
            ))
      ],
    );
  }

  void _showDeleteDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Icon(Icons.warning_amber_outlined),
            content: const Text('퇴사 처리하시겠습니까?'),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('취소')),
              TextButton(
                  onPressed: () {
                    _delMember();
                  },
                  child: const Text('확인')),
            ],
          );
        });
  }
}
