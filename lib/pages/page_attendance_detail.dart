import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:groupware_manager_front/components/component_cupertino.dart';
import 'package:groupware_manager_front/components/component_notification.dart';
import 'package:groupware_manager_front/components/component_refresh_button.dart';
import 'package:groupware_manager_front/components/component_title_design.dart';
import 'package:groupware_manager_front/model/attendance/attendance_detail.dart';
import 'package:groupware_manager_front/repository/repo_attendance.dart';

class PageAttendanceDetail extends StatefulWidget {
  const PageAttendanceDetail({super.key, required this.memberId});

  final int memberId;

  @override
  State<PageAttendanceDetail> createState() => _PageAttendanceDetailState();
}

class _PageAttendanceDetailState extends State<PageAttendanceDetail> {
  AttendanceDetail _item = AttendanceDetail(0, '', '', '', '');

  @override
  void initState() {
    super.initState();

    _getAttendanceDetail();
  }

  Future<void> _getAttendanceDetail() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoAttendance().getAttendanceDetail(widget.memberId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _item = res.data!;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '처리 실패',
        subTitle: '네트워크가 원활하지 않습니다. \n다시한번 시도해주세요.',
      ).call();

      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        Container(
          margin: const EdgeInsets.fromLTRB(24, 45, 0, 20),
          child: Row(
            children: [
              ComponentTitleDesign(title: '${_item.memberName}님'),
              ComponentRefreshButton(
                action1Callback: () {
                  _getAttendanceDetail();
                },
                action1Icon: Icons.refresh_outlined,
              )
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('사원명'),
            trailing: Text('${_item.memberName}'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('기준일'),
            trailing: Text('${_item.dateWork}'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('오늘상태'),
            trailing: Text('${_item.attendanceType}'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('출근시간'),
            trailing: Text('${_item.timeStart}'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('퇴근시간'),
            trailing: Text('${_item.timeLeave}'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('조퇴시간'),
            trailing: Text('${_item.timeEarlyLeave}'),
          ),
        ),
      ],
    );
  }
}
