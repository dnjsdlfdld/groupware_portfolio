import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:groupware_manager_front/components/component_cupertino.dart';
import 'package:groupware_manager_front/components/component_notification.dart';
import 'package:groupware_manager_front/components/component_refresh_button.dart';
import 'package:groupware_manager_front/components/component_title_design.dart';
import 'package:groupware_manager_front/model/holiday/holiday_detail.dart';
import 'package:groupware_manager_front/repository/repo_holiday.dart';
import 'package:intl/intl.dart';

class PageHolidayDetail extends StatefulWidget {
  PageHolidayDetail({super.key, required this.uploadId});

  final int uploadId;

  @override
  State<PageHolidayDetail> createState() => _PageHolidayDetailState();
}

class _PageHolidayDetailState extends State<PageHolidayDetail> {
  final DateFormat dFormat = DateFormat('yyyy-MM-dd');
  final DateFormat dateFormat = DateFormat('yyyy-MM-dd HH:mm:ss');

  HolidayDetail _item = HolidayDetail(
      0, '', '', '', '', DateTime.now(), DateTime.now(), '', '', '', '');

  @override
  void initState() {
    super.initState();
    _loadItem();
  }

  Future<void> _loadItem() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoHoliday().getDetailList(widget.uploadId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _item = res.data;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        elevation: 0,
        actions: [
          IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: const Icon(Icons.close_sharp, color: Colors.black)),
        ],
      ),
      body: _buildBody(),
      backgroundColor: Colors.white,
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        Container(
          margin: const EdgeInsets.fromLTRB(24, 45, 0, 20),
          child: Row(
            children: [
              ComponentTitleDesign(title: '${_item.memberFullName}님의 상세정보'),
              ComponentRefreshButton(
                action1Callback: () {
                  _loadItem();
                },
                action1Icon: Icons.refresh_outlined,
              )
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('등록번호'),
            trailing: Text('${_item.uploadId}'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('사원명 및 소속'),
            trailing: Text('${_item.memberFullName}'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('제목'),
            trailing: Text('${_item.title}'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('내용'),
            trailing: Text('${_item.content}'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('휴가구분'),
            trailing: Text('${_item.holidayType}'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('시작일'),
            trailing: Text(dFormat.format(_item.startHoliday)),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('종료일'),
            trailing: Text(dFormat.format(_item.endHoliday)),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('사용일수'),
            trailing: Text('${_item.useHoliday}'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('결재상태'),
            trailing: Text('${_item.approvalState}'),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('승인일자'),
            trailing: Text(_item.approvalDate),
          ),
        ),
        const Divider(thickness: 1.5, height: 4, indent: 27, endIndent: 25),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ListTile(
            title: const Text('반려일자'),
            trailing: Text(_item.companionDate),
          ),
        ),
      ],
    );
  }
}
