import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:groupware_manager_front/components/component_cupertino.dart';
import 'package:groupware_manager_front/components/component_notification.dart';
import 'package:groupware_manager_front/components/component_title_design.dart';
import 'package:groupware_manager_front/config/config_form_validator.dart';
import 'package:groupware_manager_front/model/member_manage/member_manage_detail_item.dart';
import 'package:groupware_manager_front/model/member_manage/member_put_company_info.dart';
import 'package:groupware_manager_front/repository/repo_member.dart';

class PageMemberPutCompanyForm extends StatefulWidget {
  PageMemberPutCompanyForm({super.key, this.memberManageDetailItem});

  MemberManageDetailItem? memberManageDetailItem;

  @override
  State<PageMemberPutCompanyForm> createState() =>
      _PageMemberPutCompanyFormState();
}

class _PageMemberPutCompanyFormState extends State<PageMemberPutCompanyForm> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _putMemberCompany(
      int memberId, MemberPutCompanyInfo companyInfo) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoMember().putMemberCompany(memberId, companyInfo).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '수정되었습니다.',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();

      Navigator.pop(context);
      Navigator.pop(context, [true]);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '수정 실패',
        subTitle: '수정 내용을 다시한번 확인해주세요.',
      ).call();

      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        elevation: 0,
        actions: [
          IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: const Icon(Icons.close_sharp, color: Colors.black)),
        ],
      ),
      backgroundColor: Colors.white,
      body: Column(children: [
        Container(
          margin: const EdgeInsets.fromLTRB(30, 60, 0, 30),
          child: const ComponentTitleDesign(title: '인사정보수정'),
        ),
        _buildBody(),
      ]),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
        child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            initialValue: {
              'department': widget.memberManageDetailItem!.department,
              'position': widget.memberManageDetailItem!.position,
            },
            child: Container(
              margin: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Column(
                children: [
                  const SizedBox(height: 25),
                  Row(children: [
                    Text(
                      '등록번호     ${widget.memberManageDetailItem!.id}',
                      style: const TextStyle(
                          fontSize: 15.5, color: Colors.black54),
                    ),
                  ]),
                  const Divider(thickness: 1.2, color: Colors.black26),
                  const SizedBox(height: 10),
                  FormBuilderTextField(
                    textInputAction: TextInputAction.next,
                    name: 'position',
                    decoration: const InputDecoration(
                        labelText: "직급",
                        helperText:
                            'ex) CEO, VICE_PRESIDENT, DIRECTOR, \n      CONDUCTOR, SECTION_CHIEF, DEPUTY, EMPLOYEE'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(3,
                          errorText: formErrorMinLength(3)),
                      FormBuilderValidators.maxLength(10,
                          errorText: formErrorMaxLength(10)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(height: 10),
                  FormBuilderTextField(
                    textInputAction: TextInputAction.next,
                    name: 'department',
                    decoration: const InputDecoration(labelText: "부서"),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(3,
                          errorText: formErrorMinLength(3)),
                      FormBuilderValidators.maxLength(10,
                          errorText: formErrorMaxLength(10)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        TextButton(
                            style: TextButton.styleFrom(
                              foregroundColor: Colors.black,
                            ),
                            onPressed: () {
                              _showDialog();
                            },
                            child: const Text('수정')),
                      ],
                    ),
                  )
                ],
              ),
            )));
  }

  void _showDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('잠깐 !!'),
            content: const Text('입력하신 내용으로 수정하시겠습니까?'),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('취소')),
              TextButton(
                  onPressed: () {
                    if (_formKey.currentState?.saveAndValidate() ?? false) {
                      MemberPutCompanyInfo request = MemberPutCompanyInfo(
                        _formKey.currentState!.fields['department']!.value,
                        _formKey.currentState!.fields['position']!.value,
                      );
                      print(request);

                      _putMemberCompany(
                          widget.memberManageDetailItem!.id, request);
                    }
                  },
                  child: const Text('확인')),
            ],
          );
        });
  }
}
