import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:groupware_manager_front/components/component_notification.dart';
import 'package:groupware_manager_front/pages/page_login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TokenLib {
  static Future<String?> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token');
  }

  static void setToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', token);
    print(prefs);
  }

  static void logout(BuildContext context) async {
    BotToast.closeAllLoading();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', '');

    ComponentNotification(
      success: false,
      title: '로그아웃',
      subTitle: '로그아웃 되었습니다.',
    ).call();

    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
  }
}